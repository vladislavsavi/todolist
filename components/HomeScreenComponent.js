import React from 'react';
import { View } from 'react-native';
import { RkButton, RkCard, RkText } from 'react-native-ui-kitten';
import Icon from 'react-native-vector-icons/EvilIcons';

const users = [
    {name: 'Влад', id: '1'},
    {name: 'Зарина', id: '2'},
    {name: 'Общий', id: '3'}
]

export default class HomeScreen extends React.Component {

    static navigationOptions = {
        title: 'User List',
        headerTitleStyle: {
            textAlign: 'center'
        }
    };

    render() {

        const buttonsUser = users.map((item) => {
            return(
                <RkButton key={ item.id } onPress={() => this.props.navigation.navigate('TodoList', { name: item.name })} 
                    style={{ marginBottom: 20 }} rkType='accent'>
                    <Icon name="user" color='#fff' size={35}/>
                    { item.name }
                </RkButton>
            );
        })

        return (
            <View style={{flex: 1}}>
                <RkCard rkType='story' style={{flex: 1, flexDirection: 'column'}}>
                    <View rkCardHeader>
                        <RkText style={{ fontSize: 25 }} rkType="header">Choose User</RkText>
                    </View>
                    <View rkCardContent style={{ width: '100%'}}>
                        { buttonsUser }
                    </View>
                </RkCard>
            </View>
        );
    }}

