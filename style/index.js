import { RkTheme } from 'react-native-ui-kitten';


let fontColor = '#fff';
let hederButtonsColor = '#fff';

RkTheme.setType('RkButton', 'accent', {
  container: {
    width: '100%'
  },
  content:{
    fontSize: 20
  }
});

RkTheme.setType('RkCard', 'story', {
    header: {
      alignSelf: 'center'
    },
    content:{
      alignSelf:'center',
    },
    footer: {
        alignSelf: 'center'
    }
  });

  RkTheme.setType('RkCard', 'choose-block', {
    header: {
      alignSelf: 'center'
    },
    content:{
      alignSelf:'flex-start',
      justifyContent: 'flex-start'
    },
    footer: {
        alignSelf: 'flex-starts'
    }
  });

  RkTheme.setType('RkButton', 'button-icon', {
    backgroundColor: "#fff",
    width: 50,
  });