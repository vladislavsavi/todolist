import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { RkCard, RkText, RkButton, RkChoice, RkChoiceGroup, choiceTrigger } from 'react-native-ui-kitten';
import { Ionicons } from '@expo/vector-icons';




export default class UsersToDOList extends React.Component {

     static navigationOptions = ({ navigation }) => {
         return {
             title: navigation.getParam('name'),
             headerRight: (
                <RkButton
                onPress={() => alert('This is a button!')}
                rkType="header-icon">
                    <Ionicons name="md-add-circle" color='#fff' size={32} />
                </RkButton>
             )
         }
     }

    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('name');
        return (
            <View style={{flex: 1 }}>
                <RkCard rkType='choose-block' style={{flex:1}}>
                    <View rkCardHeader>
                        <View rkCardHeader>
                            <RkText style={{ fontSize: 25 }} rkType="header">Choose User</RkText>
                        </View>
                    </View>

                    <View rkCardContent>
                        <RkChoiceGroup selectedIndex={2} radio>
                            <TouchableOpacity>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <RkChoice rkType='radio'/>
                                    <Text>Option 1</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <RkChoice rkType='radio'/>
                                    <Text>Option 1</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <RkChoice rkType='radio'/>
                                    <Text>Option 1</Text>
                                </View>
                            </TouchableOpacity>
                        </RkChoiceGroup>
                    </View>
                </RkCard>
            </View>
        );
    }
}

