import React from 'react';
import {  View, Text } from 'react-native';
import ToDoItem from './ToDoItem';

let TodoList = ({ arrayTodo }) => {
    var outputMap = arrayTodo.map((curent , index) => {
        return <ToDoItem key={index} text={ curent.text } index= { index } />
    })
    return outputMap;
}

const PreView = (
    <View style={{flex:1, flexDirection: "row", justifyContent: "center", alignContent: "center"}}>
        <Text style={{fontSize: 20, color: '#a8aaad', alignSelf: 'center' }}>Please add your notes</Text>
    </View>
)

export const ConditionalRender = ( { arrayTodo } ) => {
    if(arrayTodo){
        return <TodoList arrayTodo={ arrayTodo }></TodoList>
    }
    else {
        return PreView
    }
}



