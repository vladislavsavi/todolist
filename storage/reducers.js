import {INCREMENT, DICREMENT} from './actions';
import initialState from './state';

export default function changeCount(state = initialState, action) {
    switch (action) {
        case 'INCREMENT':
            return {...state, count: ++state.count};
            break;
        case 'DICREMENT':
            return {...state, count: --state.count};
            break;
        default:
            return state;
    }
}