import React from 'react';
import { Provider } from 'react-redux';
import { RootStack } from './components/navigation';
import './style/index';
import store from './storage/index';



export default class App extends React.Component {
    render() {
        return (
            <RootStack/>
        );
    }
}