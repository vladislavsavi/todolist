import { createStackNavigator } from 'react-navigation';
import   HomeScreen  from './HomeScreenComponent';
import  UsersToDOList  from './UsersToDoList/UsersToDoList';

export const RootStack = createStackNavigator(
    {
        Home: HomeScreen,
        TodoList: UsersToDOList,
    },
    {
      initialRouteName: 'Home',
    }
  );
  