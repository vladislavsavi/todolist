import React from 'react';
import { ScrollView } from 'react-native';
import { ConditionalRender } from './conditionalRender';
import Icon from 'react-native-vector-icons/EvilIcons';
import Dialog from "react-native-dialog";
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';

const arrayTodo = [
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' },
    { text: 'Написать приложение' }
]

const IoniconsHeaderButton = props => (
    // the `props` here come from <Item .../>
    // you may access them and pass something else to `HeaderButton` if you like
    <HeaderButton {...props} IconComponent={Icon} />
  );


export default class UsersToDOList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            dialogVisible: false,
            dialogValue: ''
        }
    }

     static navigationOptions = ({ navigation }) => {
         return {
             title: navigation.getParam('name'),
             headerRight: (
                <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
                    <Item title="search" iconName="plus" iconSize={40} 
                    onPress={navigation.getParam('openPropmt')} />
                </HeaderButtons>
             )
         }
     }

     componentDidMount(){
         this.props.navigation.setParams({ openPropmt: this.showDialog });
     }

     showDialog = () => {
        this.setState({ dialogVisible: true, dialogValue: '' });
      };
    
      handleCancel = () => {
        this.setState({ dialogVisible: false });
      };
    
      handleDelete = () => {
        this.setState({ dialogVisible: false });
      };

    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('name');
        return (
            <ScrollView>

                <ConditionalRender arrayTodo={arrayTodo} />

                <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title>Add plans</Dialog.Title>
                        <Dialog.Description>
                            What you need to do
                        </Dialog.Description>
                        <Dialog.Input onChangeText={(dialogValue) => this.setState({dialogValue})}/>
                        <Dialog.Button label="Cancel" onPress={this.handleCancel} />
                        <Dialog.Button label="Add" onPress={this.handleDelete} />
                </Dialog.Container>

            </ScrollView>
        ) 
    }
}

