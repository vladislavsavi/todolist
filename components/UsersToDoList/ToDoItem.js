import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { RkChoice } from 'react-native-ui-kitten';

export default class ToDoItem extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            done: false
        }
    }

    chekedTodo = () => {
        this.setState((state) => {
            return { done: !state.done }
        });
    }

    render() {
        const { text, index } = this.props;
        return(
            <View style={{backgroundColor: '#fff',
              marginBottom: 2, flexDirection: "row",
              flex: 1, paddingHorizontal: 15, paddingVertical: 10}}>
            <View style={{flex:10}}>
                <TouchableOpacity style={{flexDirection: "row"}} onPress={this.chekedTodo}>
                    <RkChoice style={{marginRight: 10, alignSelf: "center"}} onChange={this.chekedTodo} selected={this.state.done}/>
                    <Text style={{ fontSize: 15, 
                        alignSelf: "center",
                         textDecorationLine: this.state.done ? 'line-through' : 'none' }}>
                        {index + 1}.) { text }
                    </Text>
                </TouchableOpacity>
            </View>
                <View style={{flex: 2, flexDirection: "row", justifyContent: "space-between", paddingRight: 15}}>
                    <TouchableOpacity style={{justifyContent: "center"}}>
                        <Icon size={40} name='pencil'/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{justifyContent: "center"}}>
                        <Icon size={40} name='trash'/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

